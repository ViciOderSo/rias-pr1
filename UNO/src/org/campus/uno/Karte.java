package org.campus.uno;

public class Karte
{
	private Farbe farbe;
	private int zahl;

	public Karte(Farbe kartenfarbe, int kartenzahl)
	{
		farbe = kartenfarbe;
		zahl = kartenzahl;
	}

	public boolean match (Karte vergleich)
	{
		if (farbe == vergleich.farbe)
		{
			return true;
		}
		
		if (zahl == vergleich.zahl)
		{
			return true;
		}
		
		return false;
	}
	

	public boolean compare (Karte zweiteKarte)
	{
		if (farbe == zweiteKarte.farbe) // wenn die Farben gleich sind
		{
			// vergleicht die zahlen
			boolean result = zahl > zweiteKarte.zahl; 
			return result;
			
//			return zahl <zweiteKarte.zahl; // kurze schreibweise
			
		}
		else 
		{	// ordinal gibt einen zahlenwert mit der stelle im enum zur�ck
			return farbe.ordinal() <zweiteKarte.farbe.ordinal();
		}
		}
	
	public String toString()
	{
		return farbe.toString() + zahl;
	}
}