import java.util.ArrayList;

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenArray
{

	public static void main(String[] args)
	{

		// int[] zahlen = new int[] {1,2,3,4}

		Karte[] meineKarten = new Karte[]
		{ new Karte(Farbe.blau, 1), new Karte(Farbe.rot, 1), new Karte(Farbe.gelb, 10) };

		System.out.println(meineKarten[1]);

		for (Karte karte : meineKarten)
		{
			System.out.println(karte);
		}

		System.out.println();

		// Type Arraylist die Karten verwalten kann
		ArrayList<Karte> meineListe = new ArrayList<Karte>();

		// eine Karte in die Liste einf�gen
		meineListe.add(new Karte(Farbe.gruen, 2));
		meineListe.add(new Karte(Farbe.blau, 2));
		meineListe.add(new Karte(Farbe.rot, 3));

		// gibt mir die Karte an der Position 1
		System.out.println(meineListe.get(1));
		System.out.println();

		// eine liste mit einer schleife ausgeben
		for (Karte karte : meineListe)
		{
			System.out.println(karte);
		}
		System.out.println(meineListe.size());
		
		System.out.println(meineListe.remove(0));
		
		System.out.println(meineListe.size());
		
		System.out.println(meineListe);
	}

}
