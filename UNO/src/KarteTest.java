

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KarteTest
{
	public static void main(String[] args)
	{
		Karte karte1 = new Karte(Farbe.rot, 2);
		System.out.println(karte1);
		System.out.println();
		Karte karte2 = new Karte (Farbe.gruen, 5);
		System.out.println(karte2);
		System.out.println();
		Karte karte3 = new Karte (Farbe.rot, 3);
		System.out.println(karte3);
		System.out.println();
		
		System.out.println(karte1.match(karte2));
		System.out.println(karte1.match(karte3));
	}
	
	
}
