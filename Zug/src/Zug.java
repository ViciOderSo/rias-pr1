
public class Zug
{
	private Wagon erster;
	private Wagon letzter;
	private int zuglaenge = 0; // standartm��ig ist es auf null

	public Zug()
	{
	}

	public void neuerWagon(String inhalt)
	{
		Wagon neuerWagon = new Wagon(inhalt);

		// der zug ist leer
		if (erster == null) // null ist die referenz dass noch ins leere gezeigt wird
		{
			erster = neuerWagon;
			letzter = neuerWagon;
		} else // es gibt zumindest schon einen wagon
		{
			letzter.einhaengen(neuerWagon);
			letzter = neuerWagon;
		}
			zuglaenge++;
	}
	
			public String wagonInhaltAnStelle(int stelle)
			{
				if (stelle >zuglaenge-1 || stelle <0) // || <-- hei�t oder
				{
					return "Stelle nicht vorhanden";
				}
				
				Wagon temp = erster;
				for (int springen =0; springen < stelle; springen++)
				{
					temp=temp.nachbar(); // zum n�chsten springen
				}
				
				return temp.toString();
			}
	

	public int length()
	{
		return zuglaenge;
	}

	public String toString()
	{
		if (erster == null)
		{
			return "";
		} else
		{
			String ergebnis = "" + zuglaenge + ":";

			Wagon temp = erster;
			while (temp != null)
			{
				ergebnis = ergebnis + " " + temp.toString();
				temp = temp.nachbar();
			}

			return ergebnis;
		}
	}
}
