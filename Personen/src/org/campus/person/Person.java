package org.campus.person;

import java.util.ArrayList;

public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;
	private ArrayList<Person>Kind = new ArrayList<Person>();
	
	

	public Person(String vorname, String nachname)
	{
		this.vorname = vorname;
		this.nachname = nachname;
	}
	
	public void setKind (Person Kind)
	{
		Kind.setKind(Kind);
	}

	public void setVater(Person vater)
	{
		this.vater = vater;
	}

	public void setMutter(Person mutter)
	{
		this.mutter = mutter;
	}

	public Person getMutter() // nicht zwingend notwendig
	{
		return mutter;
	}

	public Person getVater()
	{
		return vater;
	}
	
	
	public String toString()
	{
		return String.format("(%s %s)(%s %s)(%s)", vorname, nachname,mutter, vater, Kind); // return vorname + " " + nachname
	}
}
