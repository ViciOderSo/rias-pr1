import java.util.LinkedList;
import java.util.ListIterator;


import org.campus02.student.Student;

public class DemoStudent {

	public static void main(String[] args) {
		
		Student s1 = new Student ("Max", 21);
		Student s2 = new Student ("Peter", 33);
		Student s3 = new Student ("John", 20);
		Student s4 = new Student ("Phil", 18);
		
 
		LinkedList <Student> student = new LinkedList<Student> ();
		
		student.addFirst(s1);
		student.addFirst(s2);
		student.addFirst(s3);
		student.addFirst(s4);
	
		ListIterator<Student> iterator = student.listIterator ();
		iterator.next();
		iterator.next();
		iterator.add(new Student ("Marie", 25));
		iterator.add(new Student ("Nena", 17));
		iterator.next();
		iterator.remove();
		
		iterator = student.listIterator();
		while (iterator.hasNext())
		{
			System.out.println(iterator.next());
		}
		
		Student st1 = new Student ("Max",21);
		Student st2 = new Student ("Max",21);
	
		boolean istGleich =st1.equals(st2);
		System.out.println(istGleich);
		
		
	}

}
