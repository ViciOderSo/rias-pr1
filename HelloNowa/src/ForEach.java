
public class ForEach
{

	public static void main(String[] args)
	{
		int[] zahlen = new int[]
		{ 1, 5, 12, 54, 24, 45, 4, 8, 20, 41 };

		// name der Variable : feld das es durchlaufen will
		for (int wert : zahlen) // die wird so oft durchlaufen wie das feld lang ist
		{
			System.out.println(wert); // bei jedem durchlauf hat wert den Zahlenwert
										// an der stelle im feld
		}

		int summe = 0;
		for (int zahlenwert : zahlen)
		{
			summe = summe + zahlenwert;
		}
		System.out.println(summe);
		
		
		// for (int stelle = 0; stelle < zahlen.length; stelle++) // ist das gleiche wie
		// oben nur in lang
		// {
		// int wert = zahlen[stelle];
		// }

	}

}
