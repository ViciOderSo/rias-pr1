
public class ArrayExample
{

	public static void main(String[] args)
	{
		int[] feld1 = new int[]
		{ 5, 2, 4, 1, 3, 8, 6, 2, 3, 4, 6 };

		int stelle = 4;
		System.out.println(feld1[stelle]); // index aus einer variable

		System.out.println(feld1[3]); // wert an der stelle 3;

		System.out.println(feld1.length); // l�nge vom feld

		printArray(feld1);
		System.out.println();

		printArray(new int[]
		{ 1, 2, 3, 4, 5, 6, 7 });

		System.out.println();
		printArray(feld1); // ausgabe vor dem tauschen
		swapPosition(feld1, 6, 1); // tauschen
		printArray(feld1); // nach dem tauschen

	}

	public static void printArray(int[] feld1)
	{
		for (int index = 0; index < feld1.length; index++) // alle inhalte aus dem feld ausgeben
		{
			System.out.print(feld1[index] + " ");
		}
		System.out.println();
	}

	// schreiben sie eine methode die in eine mFeld zwei stellen vertauscht
	// die methode hat das feld und die erste und zweite stelle als parameter

	public static void swapPosition(int[] feld, int a, int b)
	{
		int temp = feld[a]; // wert an erster stelle merken
		feld[a] = feld[b]; // wert von zweiter stelle auf erste schreiben
		feld[b] = temp; // gemerkten wert an die zweite stelle schreiben
	}
}
