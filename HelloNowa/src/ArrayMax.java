
public class ArrayMax
{

	public static void main(String[] args)
	{

		int[] a = new int[]	{ 2, 3, 5, 1, 3, 7, 10, 23, 1 };
		int max = findMax(a);
		System.out.println(max);
	}

	public static int findMax(int[] feld)
	{
		int result = feld[0]; // das erste maximum ist der erste wert aus dem feld
		for (int index = 0; index < feld.length; index++)
		{
			if (result < feld[index])
			{
				result = feld[index];
			}
		}
		return result;
	}
}
