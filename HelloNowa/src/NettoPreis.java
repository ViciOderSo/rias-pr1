
public class NettoPreis
{

	public static void main(String[] args)
	{
		double netto = berechneNettoPreis(200, 2);
		
		System.out.println(netto );
		
		berechneNettoPreis (100, 1);
		
		double brutto = BruttoNettoKategorie.bruttoPreis(200, 2);
		System.out.println(brutto);
	}

	
	// methode aus einem bruttoPreis einen netto Preis berechnet
	
	public static double berechneNettoPreis (double bruttoPreis, int steuerKategorie)
	{	
		double result;
		
		switch (steuerKategorie)
		{
		case 1:
			result = bruttoPreis /1.2;
			break;
		case 2:
			result = bruttoPreis /1.12;
			break;
		case 3:
			result = bruttoPreis /1.1;
			break;
		default:
			result = bruttoPreis;
		}
		
		
		System.out.println(bruttoPreis + " " + steuerKategorie + " " + result);
		
		return result;
	}
}
