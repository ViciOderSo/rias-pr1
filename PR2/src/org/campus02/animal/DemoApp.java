package org.campus02.animal;

public class DemoApp {

	public static void main(String []Args) {
	
		
		Dog dog1 = new Dog ();
		dog1.name = " Peppi \n";
		dog1.makeNoise();
		dog1.move();
		
		
		Cat cat1 = new Cat();
		cat1.name = " Mitzi \n";
		cat1.makeNoise();
		cat1.move();
		
		
		Beagle beagle = new Beagle ();
		beagle.loveFood = "Knochen";
		beagle.name="Beagle";
		beagle.makeNoise();
}
}
