package org.campus02.blackjack;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class BlackJack {
	
	private HashMap <Player, Integer> players = new HashMap<>();
	
	
	public boolean add (Player player)
	{
		if (!players.containsKey(player)) {
			players.put(player, 0);
			return true;
		}
		return false;
	}
	
	public boolean addCard (Player player, Integer cardValue)
	{
		if (players.containsKey(player)) {
			int value = players.get(player);
			players.put(player, value + cardValue);
			return true;
		}
		return false;
	}
	
	public Integer getValue (Player player)
	{
		return players.get(player);
	}

	@Override
	public String toString() {
		String text = "";
		Set<Entry<Player, Integer>> playersSet = players.entrySet();
		for (Entry<Player, Integer> entry : playersSet) {
			text += entry.getKey().getName() + ", " + entry.getValue() + "\n";
		}
		return text;
	}
	
	
	

}
