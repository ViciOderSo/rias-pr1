import org.campus02.blackjack.BlackJack;
import org.campus02.blackjack.Player;

public class DemoBJ {

	public static void main(String[] args) {

		Player p1 = new Player ("Max Mustermann",20);
		Player p2 = new Player ("Susi Sorglos",25);
		Player p3 = new Player ("Maria Mimi",45);
		
	
		
		BlackJack bj = new BlackJack();
		bj.add(p1);
		bj.add(p2);
		bj.add(p3);
		
		System.out.println("Default Werte");
		System.out.println(bj);
		
		bj.addCard(p1, 3);
		bj.addCard(p2, 10);
		bj.addCard(p3, 4);
		
		System.out.println("HashMap Manipulation");
		System.out.println(bj);
		
		bj.addCard(p1, 14);
		bj.addCard(p2, 10);
		bj.addCard(p3, 30);
		
	
	}

}
