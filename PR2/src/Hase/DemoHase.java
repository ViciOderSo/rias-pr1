package Hase;

public class DemoHase {

	public static void main(String []args) {
		Hase hase1 = new Hase();
		hase1.name = "Klopfer";
		hase1.hoppeln();
		hase1.schlafen();
		hase1.fressen();
		
		Hase hase2 = new Hase();
		hase2.name = "Osterhase";
		hase2.hoppeln();
		hase2.ostereier_verstecken();
		
		Hase hase3 = new Hase();
		hase3.name = "Weihnachtshase";
		hase3.hoppeln();
		hase3.geschenke_verteilen();
	}
}
