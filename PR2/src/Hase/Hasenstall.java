package Hase;

import java.util.ArrayList;

public class Hasenstall {
	protected ArrayList<Hase> hasen = new ArrayList<>();

	public void addHase(Hase hase) {
		hasen.add(hase);
	}

	public void fressen() {
		for (Hase hase : hasen) {
			hase.fressen();
		}
	}
	
	public void schlafen() {
		for (Hase hase : hasen) {
			hase.schlafen();
		}
	}
}
