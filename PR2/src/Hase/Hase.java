package Hase;

import org.campus02.animal.animal;

public class Hase extends animal {

	@Override
	public void schlafen() {
		System.out.println(name + " schl�ft");
	}

	@Override
	public void hoppeln() {
		System.out.println(name + "hoppelt");
	}

	@Override
	public void fressen() {
		System.out.println(name + " frisst");
	}

	@Override
	public void geschenke_verteilen() {
		System.out.println(name + " verteilt Geschenke");
	}

	@Override
	public void ostereier_verstecken() {
		System.out.println(name+ " versteckt Ostereier");
	}

}
