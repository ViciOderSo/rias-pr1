import java.util.Scanner;

public class ConsoleTest
{

	public static void main(String[] args)
	{

		System.out.println("eingabe:");
		
		Scanner vonConsole = new Scanner(System.in);
		
		int vonConsoleGelesen = vonConsole.nextInt();
		
		System.out.println(vonConsoleGelesen);
		
		System.out.print("geben sie ihren namen ein:");
		
		String name = vonConsole.next();
		System.out.printf("Hallo %s du hast vorher %d eingegeben" , name, vonConsoleGelesen);
		
		vonConsole.close();
	}

}
