
public class Urlaubswunsch
{
	private String Ort;
	private int Reihung;
	private double Preis;

	public String getOrt()
	{
		return Ort;
	}

	public void setOrt(String ort)
	{
		Ort = ort;
	}

	public int getReihung()
	{
		return Reihung;
	}

	public void setReihung(int reihung)
	{
		Reihung = reihung;
	}

	public double getPreis()
	{
		return Preis;
	}

	public void setPreis(double preis)
	{
		Preis = preis;
	}

}
