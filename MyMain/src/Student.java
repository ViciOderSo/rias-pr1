import java.util.ArrayList;

public class Student
{
	private int StudentID;
	private String Vorname;
	private String Nachname;
	private double Credits;
	private boolean Inskribiert;
	private String Lieblingsfarbe;
	

	public int getStudentID()
	{
		return StudentID;
	}

	public void setStudentID(int studentID)
	{
		StudentID = studentID;
	}

	public String getVorname()
	{
		return Vorname;
	}

	public void setVorname(String vorname)
	{
		Vorname = vorname;
	}

	public String getNachname()
	{
		return Nachname;
	}

	public void setNachname(String nachname)
	{
		Nachname = nachname;
	}

	public double getCredits()
	{
		return Credits;
	}

	public void setCredits(double credits)
	{
		Credits = credits;
	}

	public boolean isInskribiert()
	{
		return Inskribiert;
	}

	public void setInskribiert(boolean inskribiert)
	{
		Inskribiert = inskribiert;
	}

	public String getLieblingsfarbe()
	{
		return Lieblingsfarbe;
	}

	public void setLieblingsfarbe(String lieblingsfarbe)
	{
		Lieblingsfarbe = lieblingsfarbe;
	}
	
	private ArrayList<Urlaubswunsch> Urlaubswuensche;


	public ArrayList<Urlaubswunsch> getUrlaubswuensche()
	{
		return Urlaubswuensche;
	}

	public void setUrlaubswuensche(ArrayList<Urlaubswunsch> urlaubswuensche)
	{
		Urlaubswuensche = urlaubswuensche;
	}
}
