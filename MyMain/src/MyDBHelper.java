import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager
					.getConnection("jdbc:ucanaccess://C:/Users/victoria.cihal/Desktop/DB/Urlaubsverwaltung.accdb");

			System.out.println("Super - hat funktioniert :-) ");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public void insertUrlaub(String destination, double preis)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void insertUrlaub(Urlaub urlaub)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void deleteUrlaub(int urlaubsNr)
	{

	}

	/*
	 * public ArrayList<String> GetAlleUrlaubPreisGroesserAls(double preis) {
	 * 
	 * return null; }
	 */

	public void printStudendAndUrlaube(int studentId, double preisGroesserAls)
	{
		String query = "SELECT Vorname FROM Student WHERE studentId=? ";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				String vorname = rs.getString(1);
				System.out.printf("Student %s", vorname);

				String queryUrlaube = "SELECT preis, Ort from Urlaubswunsch where preis>? and studentId=?";

				PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
				stmtFuerUrlaube.setDouble(1, preisGroesserAls);
				stmtFuerUrlaube.setInt(2, studentId);
				ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();
				int counter = 0;
				while (rsUrlaube.next())
				{
					counter++;
					double preis = rsUrlaube.getDouble(1);
					String ort = rsUrlaube.getString(2);

					System.out.printf("Preis %.2f Ort %s \n", preis, ort);
				}
				if (counter == 0)
				{
					System.out.println("Dieser Student hat keine Urlaubswuensche ");
				}
			} else
			{
				System.out.printf("Student %d nicht gefunden ", studentId);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void printUrlaubsuebersichtForAllStudents()
	{

	}

	public void printDetailsForStudentWithId(int ID)
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT vorname, nachname FROM Student WHERE StudentID = " + ID;
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);

				System.out.printf("Details zum Studenten: \n Vorname: %s Nachname: %s \n", vorname, nachname);
			}

			else
			{
				System.out.printf("Dieser ID ist kein Student zugewiesen! \n");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();

		}
	}

	public void printAllUrlaubeForStudentWithIdAndPriceGrearterThan(int studentId, double preis)
	{
		String query = "SELECT Preis, ort FROM urlaubswunsch " + " WHERE Preis > ? " + " and studentId =? "
				+ " ORder BY Ort ";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setDouble(1, preis);
			stmt.setInt(2, studentId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				double Preis = rs.getDouble(1);
				String ort = rs.getString(2);

				System.out.printf("Preis %.2f Ort %s \n", preis, ort);
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();

		}
	}

	public void printDetailsForStudentWithIdWithPreparedStatement(int studentID)
	{
		String query = "SELECT vorname, inskribiert From Student " + " WHERE studentid = ? ";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentID);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				String vorname = rs.getString(1);
				boolean inskribiert = rs.getBoolean(2);

				System.out.printf("Details zum Studenten: \n Vorname: %s inskiribiert: %s \n", vorname, inskribiert);
			}

			else
			{
				System.out.printf("Dieser ID ist kein Student zugewiesen! \n");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();

		}
	}

	public void printAllUrlaubeWithPreisGreaterThanOrderByOrt(double preis)
	{
		String query;
		try
		{
			Statement stmt = con.createStatement();
			query = " SELECT DISTINCT Ort, Preis FROM Urlaubswunsch WHERE Preis > " + preis + " ORDER BY Ort ";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String ort = rs.getString(1);
				double Preis = rs.getDouble(2);

				System.out.printf("Ort: %s, Preis: %.2f \n", ort, Preis);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printZusammenfassungProStudent()
	{
		System.out.println("-------- durchschnittlicher Preis und Anzahl der Urlaube pro Student --------");

		String query = "SELECT Student.Vorname, Student.nachname, avg(Urlaubswunsch.Preis), count(Urlaubswunsch.StudentID) "
				+ "FROM Student LEFT JOIN Urlaubswunsch ON Student.StudentID = Urlaubswunsch.StudentID GROUP BY Vorname, Nachname";
		// muss
		try
		{
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{

				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				double avg = rs.getDouble(3);
				int sum = rs.getInt(4);

				System.out.printf("%s %s: durchschnittlicher Preis: %.2f ?, Anzahl der Urlaube: %d \n", vorname,
						nachname, avg, sum);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printAllStudents()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT vorname, inskribiert FROM Student";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String vorname = rs.getString(1);
				boolean inskribiert = rs.getBoolean(2);

				System.out.printf("Vorname %s inskribiert %s \n", vorname, inskribiert);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllUrlaube()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Preis, ort, Preis *1.1 FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String ort = rs.getString(2);
				double preis = rs.getDouble(1);
				double preisPlus10Prz = rs.getDouble(3);

				System.out.printf("Ort %s Preis %f \n", ort, preis);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{

		return null;
	}

	public int updateStudent(int studentId, double newCredit)
	{
		int affectedRows = 0;
		String updateString = "";
		updateString = "UPDATE Student SET ";
		updateString += " Vredits = ? ";
		updateString += " WHERE Studentid=?";

		try
		{
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			stmtUpdate.setDouble(1, newCredit);
			stmtUpdate.setInt(2, studentId);
			affectedRows = stmtUpdate.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return affectedRows;
	}

	public int addNewStudent(Student newStudent)
	{
		int affectedRows = 0;
		String insertString = "";
		insertString = "INSERT INTO Student(Vorname, Credits) ";
		insertString += " VALUES (?,?)";
		int newIdentity = 0;
		try
		{
			PreparedStatement stmtInsert = con.prepareStatement(insertString);
			stmtInsert.setString(1, newStudent.getVorname());
			stmtInsert.setDouble(2, newStudent.getCredits());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "Select @@identity ";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);
			rs.next();
			newIdentity = rs.getInt(1);

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return newIdentity;

	}

	public void listKunden()
	{

		try
		{

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM Kunde ";

			ResultSet rs = stmt.executeQuery(sql);

			System.out.println("Meine Kunden");
			while (rs.next())
			{
				System.out.print("Kdnr: " + rs.getInt(1));
				System.out.println(" Vorname " + rs.getString(2));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void printColumnNamesForTableStudentFromMetadata()
	{
		try {
			
		ResultSet rs = con.createStatement().executeQuery( "SELECT StudentId, Vorname, Inskribiert FROM Student " );
		
	      ResultSetMetaData meta = rs.getMetaData();

	      

	      for ( int i = 1; i <= meta.getColumnCount(); i++ )
	      {
	        System.out.printf( "\n%-20s %-20s%n", meta.getColumnLabel( i ),
	                                            meta.getColumnTypeName( i ) );
	      }
		}
	      catch (SQLException e) {
	    	  e.printStackTrace();
	      }
	}

	public HashMap<String, String> getColumnNamesForTableStudentFromMetadata()
	{
		HashMap<String, String> mapColumnNameAndType = new HashMap<String,String>();
		try {
			
			ResultSet rs = con.createStatement().executeQuery( "SELECT StudentId, Vorname, Inskribiert FROM Student " );
			
		      ResultSetMetaData meta = rs.getMetaData();

		      

		      for ( int i = 1; i <= meta.getColumnCount(); i++ )
		      {
		        System.out.printf( "\n%-20s %-20s%n", 
		        		mapColumnNameAndType.put(
		        		meta.getColumnLabel( i ),
		                meta.getColumnTypeName( i ) ));
		      }
			}
		      catch (SQLException e) {
		    	  e.printStackTrace();
		      }
		return mapColumnNameAndType;
	}

	public Student getStudent(int studId)
	{
		Student s = new Student();
		
		try
		{
			String query = "SELECT StudentID, Vorname, Nachname, Credits, Inskribiert FROM Student WHERE StudentID = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()==true)
			{
				s.setStudentID(rs.getInt(1));
				s.setVorname(rs.getString(2));
				s.setNachname(rs.getString(3));
				s.setCredits(rs.getDouble(4));
				s.setInskribiert(rs.getBoolean(5));

				System.out.printf("StudentID: %d, Name: %s %s, Credits: %.2f, inskribiert: %b \n", s.getStudentID(), s.getVorname(), s.getNachname(), s.getCredits(), s.isInskribiert());
			}else
			{
				System.out.println("Student mit ID " + studId + " nicht gefunden!");
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
	}

	public Student getStudentWithUrlaubsDetails(int studentId) {
		Student s =new Student();
		s.setVorname("Josefa");
		s.setCredits(250);
		//select * from urlaubswuensche where studentid=1
		ArrayList<Urlaubswunsch> Urlaubswuensche = new ArrayList<Urlaubswunsch>();
		Urlaubswunsch u1= new Urlaubswunsch();
		Urlaubswunsch u2= new Urlaubswunsch();
		Urlaubswuensche.add(u1);
		Urlaubswuensche.add(u2);
		
		s.setUrlaubswuensche(Urlaubswuensche);
		
		return s;
		
	}
}
