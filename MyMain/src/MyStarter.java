/*import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyStarter {

	public static void main (String [] args) {
		System.out.println("Hello Campus02");
	

	try
	{
		Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
	}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	try
	{
		Connection con = DriverManager.getConnection("jdbc:ucanaccess://E:/Urlaubsverwaltung.accdb");
		System.out.println("Super - hat funktioniert :-)");
	}
	catch(SQLException e)
	{
		e.printStackTrace();
	}
}}*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyStarter {

	public static void main(String[] args)  {
		
		System.out.println("Hello Campus02");
		
		MyDBHelper helper= new MyDBHelper();
		helper.init();
		
		helper.printAllUrlaube();
		helper.printAllStudents();
		helper.printAllUrlaubeWithPreisGreaterThanOrderByOrt(600);
		helper.printDetailsForStudentWithId(5);
		helper.printDetailsForStudentWithId(1);
		helper.printDetailsForStudentWithIdWithPreparedStatement(3);
		helper.printAllUrlaubeForStudentWithIdAndPriceGrearterThan(2,200.00);
		helper.printUrlaubsuebersichtForAllStudents();
		helper.printZusammenfassungProStudent();
		helper.updateStudent(1,1);
		helper.addNewStudent(1,345);
		Student studentDetails = helper.getStudent(1);
	}
		// Vorname Anzahl UrlaubswŁnsche Durchschnitt.Preis
		// Hans 	        2	                700
		//Anna				3					450
		
		/*try {
			Class.forName("MyStarter");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		 try {
			DriverManager.getConnection("abc");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//try 
			{
			try {
				demoEins();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} */
	//catch (Exception e) {
		//	System.out.println("Stack Trace main");
			//e.printStackTrace();
		//	System.out.println("oje - ein Fehler");
		//}

	}

	
	public static void demoEins() throws Exception //Zwang - der Aufrufer MUSS ein Try Catch verwenden
	{
		System.out.println("Hello World aus Demo eins");
		
		int i=10, j;
		
		j= 0;
		
		int erg = 0;
		
		try
		{
			erg= i / j; //Kein Catch deshalb weiter hochwerfen throw
		}
		catch (Exception e)
		{
			System.out.println("Stack Trace demo eins");
			e.printStackTrace();
			throw e; 
		}
		
		System.out.println(erg);
	}
}
