
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto ("rot");
		Auto auto2 = new Auto ("blau");
		Auto auto3 = new Auto ("gelb");
		
		auto1.status();
		
		auto1.volltanken();
		auto2.volltanken();
		
		auto1.status();
		
		auto1.beschleunigen(10);
		auto3.beschleunigen(50);
		auto1.status();
		
		auto1.fahren(120);
		auto1.status();
		
	
		
	}
}
